import Login from "../Pages/Login";
import Posts from "../Pages/Posts";
import DetailPosts from "../Pages/DetailPost";
import DetailProfile from "../Pages/DetailProfile";

const pageRoutes =[
      {
        path: "login",
        element: <Login/>,
      },
      {
        path: "posts",
        element: <Posts/>,
      },
      {
        path: "posts/:id",
        element: <DetailPosts/>,
      },
      {
        path: "user/:id",
        element: <DetailProfile/>,
      },
]

export default pageRoutes