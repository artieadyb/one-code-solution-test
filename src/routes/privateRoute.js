import React from "react";
//import Cookies from "universal-cookie";
import { Navigate } from "react-router-dom";
import App from "../App";
//import { getAuthorization } from "./apiConfig";

const PrivateRoute = ({ children }) => {
  const user = sessionStorage.getItem("userLogin");
  if (!user) {
    return <Navigate to="/" />;
  }

  return <App>{children}</App>;
};

export default PrivateRoute;
