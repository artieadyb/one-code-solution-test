import Login from "../Pages/Login";
import HomePage from "../Pages/HomePage";

const loginRoutes =[
        {
        path: "/",
        element: <HomePage/>,
      },
      {
        path: "login",
        element: <Login/>,
      },
]

export default loginRoutes