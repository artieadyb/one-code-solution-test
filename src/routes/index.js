import { createBrowserRouter } from "react-router-dom";
import pageRoutes from "./pageRoutes";
import loginRoutes from "./loginRoutes";
import PrivateRoute from "./privateRoute";
import PublicRoute from "./PublicRoute";


const router = createBrowserRouter([
    {
        path: '/app',
        element:<PrivateRoute/>,
        children: [
            ...pageRoutes
         ]
    },
    {
        path: '/',
        element:<PublicRoute/>,
        children: [
            ...loginRoutes
         ]
    }
]);

export default router;