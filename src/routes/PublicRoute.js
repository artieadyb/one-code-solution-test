import '../App.css'
import { Outlet } from 'react-router-dom'
import NavigationBar from '../Components/Navbar';

function PublicRoute() {
  return (
    <div className="App">
      <NavigationBar/> 
      <div className='d-flex' style={{paddingTop:"50px"}}>
        <Outlet/>
      </div>
    </div>
  );
}

export default PublicRoute;