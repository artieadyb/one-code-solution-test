import axios from 'axios';

const services = ()=>{

    const getUser = async(param)=>{
        try {
            const data = await axios
              .get(`https://jsonplaceholder.typicode.com/users?username=${param}`,
              {
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  //Authorization: `Bearer ${accessToken}`,
                },
              }
              )
              .then(({data}) => {
                return data
              });
            return data;
          } 
          catch (err) {
            console.error(err.message);
          }
        }

    const getUserList = async()=>{
        try {
            const data = await axios
                .get(`https://jsonplaceholder.typicode.com/users`,
                {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    //Authorization: `Bearer ${accessToken}`,
                },
                }
                )
                .then(({data}) => {
                    return data
                });
                return data;
            } 
            catch (err) {
            console.error(err.message);
            }
        }

    const getPosts = async(id)=>{
        try {
            const data = await axios
                .get(`https://jsonplaceholder.typicode.com/posts?userId=${id}`,
                {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    //Authorization: `Bearer ${accessToken}`,
                },
                }
                )
                .then(({data}) => {
                    return data
                });
                return data;
            } 
            catch (err) {
            console.error(err.message);
            }
        }
    
    
    const getPostDetail = async(id)=>{
        try {
            const data = await axios
                .get(`https://jsonplaceholder.typicode.com/posts/${id}`,
                {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                        //Authorization: `Bearer ${accessToken}`,
                },
                }
                )
                .then(({data}) => {
                    return data
                });
                return data;
            } 
            catch (err) {
            console.error(err.message);
            }
        }

    const getTotalComments = async(id)=>{
        try {
            const data = await axios
                .get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`,
                {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    //Authorization: `Bearer ${accessToken}`,
                },
                }
                )
                .then(({data}) => {
                    return data
                });
                return data;
            } 
            catch (err) {
            console.error(err.message);
            }
        }

    const getUserDetail = async(id)=>{
        try {
            const data = await axios
                .get(`https://jsonplaceholder.typicode.com/users/${id}`,
                {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    //Authorization: `Bearer ${accessToken}`,
                },
            }
            )
            .then(({data}) => {
                return data
            });
            return data;
            } 
            catch (err) {
            console.error(err.message);
            }
        }

    return{
        getUser,
        getUserList,
        getPosts,
        getPostDetail,
        getTotalComments,
        getUserDetail,
    }
}

export default services