import React from 'react'
import Pagination from 'react-bootstrap/Pagination';

const PaginationBar = ({setCurrentPage, handleNext, handlePrev}) => {
  return (
    <Pagination>
      <Pagination.Prev onClick={()=>handlePrev()}/>
      <Pagination.Item onClick={()=>setCurrentPage(1)}>{1}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(2)}>{2}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(3)}>{3}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(4)}>{4}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(5)}>{5}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(6)}>{6}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(7)}>{7}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(8)}>{8}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(9)}>{9}</Pagination.Item>
      <Pagination.Item onClick={()=>setCurrentPage(10)}>{10}</Pagination.Item>
      <Pagination.Next onClick={()=>handleNext()}/>
    </Pagination>
  )
}

export default PaginationBar