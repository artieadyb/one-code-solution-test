import React from 'react'
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import { useNavigate, Link } from 'react-router-dom';

const NavigationBar = () => {
    const user = sessionStorage.getItem("userLogin");
    const userId = sessionStorage.getItem("userId");
    const navigate = useNavigate()

    const handleLogout =()=>{
      sessionStorage.clear();
      navigate('/')
    }

  return (
    <Navbar expand="lg" variant="light border-bottom shadow-sm fixed-top" bg="light">
      <Container>
        <Navbar.Brand className="fs-3 text-secondary" href="#"><span className='fw-bold text-primary'>Cinta</span> Coding</Navbar.Brand>
        {!user ? (
          <Button variant="primary rounded-pill px-4" onClick={()=>navigate(`/login`)}>Login</Button>
        ):(
          // <div className="fs-5 text-secondary">
          //   <span className='fw-bold text-primary'>Welcome, </span>{user[0]?.name}
          // </div>
          <div className="dropdown">
          <button className="btn fs-5 text-secondary" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            <span className='fw-bold text-primary'>Welcome, </span>{user}
          </button>
          <ul className="dropdown-menu">
            <li><Link className="dropdown-item" to={`/app/user/${userId}`}>Detail Profile</Link></li>
            <li><div className="btn dropdown-item" onClick={()=>handleLogout()}>Logout</div></li>
          </ul>
          </div>
        )}
      </Container>
    </Navbar>
  )
}

export default NavigationBar