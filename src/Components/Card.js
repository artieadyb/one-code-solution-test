import React from 'react'
import { Link} from 'react-router-dom'

const Card = ({data, user}) => {
    const pathName = window.location.pathname;
  return (
    <div className='row border border-1 mb-2 rounded' style={{width:'800px'}}>
        <div className='col-4 d-flex align-items-center fw-bold text-secondary'>
            <span className='mx-auto'>
                {data?.name}
            </span>
        </div>
        <div className='col-8 text-start'>
            <div>
               {data?.title}
            </div>
            <div className='d-flex gap-3'>
                <div>
                    <Link to={`${pathName}/${data?.id}`}>Detail</Link>
                </div>
            </div>
        </div>
    </div>
  )
}

export default Card