import React, {useEffect, useState} from 'react'
import {useParams, Link} from 'react-router-dom'
import services from '../Services';
import { FaRegComment } from 'react-icons/fa'

const DetailCard = ({data}) => {
    const params = useParams()
    const {getUserList, getTotalComments } = services()
    const [users, setUsers] = useState()
    const [comments, setComments] = useState()
    const nama =  users?.find((item)=>item?.id === data?.userId)

    useEffect(() => {
        getUserList().then((result) => setUsers(result));
        getTotalComments(params?.id).then((result) => setComments(result.length));
    }, []);

  return (
    <div className='row border border-1 mb-2 py-3 rounded' style={{width:'800px'}}>
        <div className='col-4 d-flex align-items-center fw-bold text-secondary'>
            <span className='mx-auto'>
                {nama?.name}
            </span>
        </div>
        <div className='col-8 text-start'>
            <div className='mb-3 text-secondary fw-bold'>
               {data?.title}
            </div>
            <div className='mb-3'>
               {data?.body}
            </div>
            <div className='mb-3'>
            <FaRegComment className='text-primary'/> <span className='text-secondary'>{comments}</span>
            </div>
            <div className=''>
                <div>
                    <Link to={`/app/posts`}>Back</Link>
                </div>
            </div>
        </div>
    </div>
  )
}

export default DetailCard