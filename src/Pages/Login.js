import React, {useContext} from 'react'
import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as Yup from "yup";
import { useNavigate } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import services from '../Services';
import DataContext from '../context/DataContext';

const Login = () => {
  const navigate = useNavigate()
  const {getUser} = services()
  const {setUser} = useContext(DataContext)

  const initialValues = {
    username: '',
    password: '',
  };

  const validation = Yup.object({
      username: Yup.string().required("Username is a required"),
      password: Yup.string().required("Password is a required"),
  });

  const onSubmit = (values) => {
    getUser(values?.username).then((result) => {
      if(result.length<=0){
        alert("User not found")
      }else{
        const user = result[0].name
        const id = result[0].id
        console.log(user)
        sessionStorage.setItem("userLogin", user);
        sessionStorage.setItem("userId", id);
        const userLoggedIn = sessionStorage.getItem("userLogin");
        setUser(userLoggedIn)
        alert("Success Login")
        navigate('/app/posts')
      }
    })
  };

  return (
    <div className='align-items-center mt-5 mx-auto'>
      <div className='fs-3 fw-bold mb-5 text-secondary'>Login</div>
      <div className='d-flex flex-column text-start border border-1 rounded-3 p-5' style={{width:"400px"}}>
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validation}
        >
          {(formik)=>{
              return(
                <Form>
                  <div className="mb-3">
                    <label className="form-label">Username</label>
                    <Field 
                      type="text" 
                      className="form-control shadow-sm" 
                      id="username" 
                      name="username" 
                      placeholder="input your username"
                    />
                    <div
                      style={{
                      color: 'red',
                      textAlign: 'left',
                      fontSize: '11px',
                      }}
                    >
                    <ErrorMessage name='username' />
                    </div>
                  </div>
                  <div className="mb-5">
                    <label className="form-label">Password</label>
                    <Field 
                      type="password" 
                      className="form-control shadow-sm" 
                      id="password" 
                      name="password" 
                      placeholder="input your password"
                    />
                    <div
                      style={{
                      color: 'red',
                      textAlign: 'left',
                      fontSize: '11px',
                      }}
                    >
                    <ErrorMessage name='password' />
                    </div>
                  </div>
                  <div className='d-flex'>
                    <div className='mx-auto'>
                    <Button 
                      type='submit'
                      className="btn btn-primary mx-auto rounded px-5" 
                    >
                      Login
                    </Button>
                    </div>
                  </div>
                </Form>
              )
          }}
        </Formik>
      </div>
    </div>
  )
}

export default Login