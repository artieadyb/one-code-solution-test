import React, {useState, useEffect, useContext} from 'react'
import Card from '../Components/Card'
import PaginationBar from '../Components/Pagination'
import services from '../Services';

const Posts = () => {
  const {getUserList,getPosts} = services()
  const [data, setData] = useState()
  const [users, setUsers] = useState()
  const [currentPage, setCurrentPage] = useState(1)

  const handleNext = () =>{
    setCurrentPage(currentPage+1)
  }
  
  const handlePrev = () =>{
    setCurrentPage(currentPage-1)
  }
  
  useEffect(() => {
    getUserList().then((result) => setUsers(result));
    getPosts(currentPage).then((result)=>{
      const postList = []
      result?.map(
        (item) => {
          const name = users?.find((data)=>data?.id === item?.userId)
          postList.push({ id:item?.id, title: item?.title, name: name?.name });
        }
        )
        setData(postList)
    });
  }, [currentPage]);
  
  return (
    <div className='align-items-center mt-5 pb-5 mx-auto'>
      <div className='fs-3 fw-bold mb-3 text-secondary'>Posts</div>
      <div className='d-flex flex-column'>
        {data?.map((item, index)=>(
          <div key={index}>
            <Card 
            data={item}
            />
          </div>
        ))}
        <div className='mx-auto mt-3'>
          <PaginationBar 
          setCurrentPage={setCurrentPage}
          handleNext={handleNext}
          handlePrev={handlePrev}
          />
        </div>
      </div>
    </div>
  )
}

export default Posts