import React, {useState, useEffect, useContext} from 'react'
import { useParams } from 'react-router-dom';
import DetailCard from '../Components/DetailCard';
import PaginationBar from '../Components/Pagination'
import services from '../Services';
//import DataContext from '../context/DataContext';

const DetailPosts = () => {
    const param = useParams()
    const {getPostDetail, getUserList } = services()
    const [data, setData] = useState()
  
  useEffect(() => {
    getPostDetail(param.id).then((result) => {
        setData(result) 
    });
  }, []);
  
  return (
    <div className='align-items-center mt-5 pb-5 mx-auto'>
      <div className='fs-3 fw-bold mb-3 text-secondary'>Posts</div>
      <div className='d-flex flex-column'>
          <div>
            <DetailCard 
            data={data}
            />
          </div>
      </div>
    </div>
  )
}

export default DetailPosts