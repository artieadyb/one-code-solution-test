import React, {useState, useEffect} from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import services from '../Services'

const DetailProfile = () => {
    const params = useParams()
    const navigate = useNavigate()
    const {getUserDetail} = services()
    const [data, setData] = useState()

    useEffect(() => {
        getUserDetail(params?.id).then((result) => setData(result));
      }, []);

  return (
    <div className='align-items-center mt-5 mx-auto'>
        <div className='fs-3 fw-bold mb-5 text-secondary'>Profile</div>
        <div className='d-flex flex-column text-start border border-1 rounded-3 p-5' style={{width:"400px"}}>
            <div className='row mb-2'>
                <div className='col-3'>Name</div>
                <div className='col-1'>:</div>
                <div className='col-8'>{data?.name}</div>
            </div>
            <div className='row mb-2'>
                <div className='col-3'>email</div>
                <div className='col-1'>:</div>
                <div className='col-8'>{data?.email}</div>
            </div>
            <div className='row mb-2'>
                <div className='col-3'>Address</div>
                <div className='col-1'>:</div>
                <div className='col-8'>{`${data?.address?.suite}, ${data?.address?.street} - ${data?.address?.city}`}</div>
            </div>
            <div className='row mb-5'>
                <div className='col-3'>Phone</div>
                <div className='col-1'>:</div>
                <div className='col-8'>{data?.phone}</div>
            </div>
            <div className='row mb-2'>
                <button className='btn btn-primary'onClick={()=>navigate('/app/posts')}>Kembali</button>
            </div>
        </div>
    </div>
  )
}

export default DetailProfile